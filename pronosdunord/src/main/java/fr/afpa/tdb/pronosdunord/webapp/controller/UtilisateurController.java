package fr.afpa.tdb.pronosdunord.webapp.controller;

import java.security.SecureRandom;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.afpa.tdb.pronosdunord.webapp.bean.Utilisateur;
import fr.afpa.tdb.pronosdunord.webapp.dao.UtilisateurDao;
import fr.afpa.tdb.pronosdunord.webapp.services.MailService;

@Controller
public class UtilisateurController {
	
	@Autowired
	private UtilisateurDao utilisateurDao;
	@Autowired
	private MailService mailService;

	@RequestMapping(value = {"/"},method = RequestMethod.GET)
	public String afficherPronistiques() {
		return "accueil";
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "do_authentification")
	public String doAuthentification(Model model, HttpSession session, String login, String motDePasse) {
		Utilisateur u = utilisateurDao.findByEmailAndPassword(login, motDePasse);
		if (u!=null) {
			session.setAttribute("connectedUser", u);
			return "redirect:pronos";
		}
		
		model.addAttribute("messageErr", "L'E-mail et/ou le mot de passe est incorrect.");
		return "connexion";
	}
	
	
	@RequestMapping(value = "do_inscription",method = RequestMethod.POST)
	public String traiterInscription(String nom, String prenom, Date dateDeNaissance, String email, String password) {
		utilisateurDao.save(new Utilisateur(nom, prenom, dateDeNaissance, email, password));
		
		return "redirect:connexion";
		
	}
	
	@GetMapping("afficher_mdp_perdu")
	public String afficherMdpPerdu() {
		return "afficher_mdp_perdu";
	}
	
	@PostMapping("do_changer_mdp")
	public String doChangerMdp(String email) {
		/*2*/Utilisateur u = utilisateurDao.findByEmail(email);
		/*3*/String nouveauPassword = generatePassayPassword(8);
		/*4*/u.setPassword(nouveauPassword);
		utilisateurDao.save(u);
		/*5*/ String[] to = {u.getEmail()};
		String objet = "Nouveau mot de passe";
		String corps = "Bonjour"+u.getNom()+"\n";
		corps = corps + "Vous avez modifié votre mot de passe :  " + nouveauPassword + "  \n";
		corps = corps + "Cordialement, \n";
		corps = corps + "Votre site  \n";
		
		String [] fichiers = {"C:\\Users\\59013-83-10\\Desktop\\all.jpg"};
		
		
		mailService.sendEmailWithAttachment(to, objet, corps,fichiers);
		
		return "redirect:connexion";
	}
	public static String generatePassayPassword(int randomStrLength) {
		char[] possibleCharacters = (new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?")).toCharArray();
		String randomStr = RandomStringUtils.random( randomStrLength, 0, possibleCharacters.length-1, false, false, possibleCharacters, new SecureRandom() );
		return randomStr ;
		}
	
	
	@PostMapping("do_contact")
	public String doChangerMdp(String email,String nom,String objet,String message) {
		/*5*/ String[] to = {email,"pronosdunord19@gmail.com"};
		
		mailService.sendEmailWithAttachment(to, objet, message);
		
		return "redirect:connexion";
	}
	
	

}
