package fr.afpa.tdb.pronosdunord.webapp.services;

import java.util.List;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
@Service
public class MailService {
	@Autowired
	private JavaMailSender javaMailSender;
	public void sendEmail(String[] to, String objet, String corps) {
		try {
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(to);
			msg.setSubject(objet);
			msg.setText(corps);
			javaMailSender.send(msg);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void sendEmailWithAttachment(String[] to, String objet, String corps, List<String> sources) {
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			// true = multipart message
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo(to);
			helper.setSubject(objet);
			helper.setText(corps, true);
			DataSource fds;
			if (sources != null) {
				for (String string : sources) {
					fds = new FileDataSource(string);
					helper.addAttachment(fds.getName(), fds);
				}
			}
			javaMailSender.send(msg);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void sendEmailWithAttachment(String[] to, String objet, String corps, String[] sources) {
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			// true = multipart message
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo(to);
			helper.setSubject(objet);
			helper.setText(corps, true);
			DataSource fds;
			if (sources != null) {
				for (String string : sources) {
					fds = new FileDataSource(string);
					helper.addAttachment(fds.getName(), fds);
				}
			}
			javaMailSender.send(msg);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void sendEmailWithAttachment(String[] to, String objet, String corps) {
		try {
			MimeMessage msg = javaMailSender.createMimeMessage();
			// true = multipart message
			MimeMessageHelper helper = new MimeMessageHelper(msg, true);
			helper.setTo(to);
			helper.setSubject(objet);
			helper.setText(corps, true);
			DataSource fds;
			
			javaMailSender.send(msg);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}