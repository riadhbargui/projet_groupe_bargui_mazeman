package fr.afpa.tdb.pronosdunord.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PronosdunordApplication {

	public static void main(String[] args) {
		SpringApplication.run(PronosdunordApplication.class, args);
	}

}
