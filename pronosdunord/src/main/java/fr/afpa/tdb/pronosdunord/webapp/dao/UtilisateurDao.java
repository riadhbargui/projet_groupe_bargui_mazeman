package fr.afpa.tdb.pronosdunord.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.tdb.pronosdunord.webapp.bean.Utilisateur;

@Repository
public interface UtilisateurDao extends JpaRepository<Utilisateur, Long>{
	
	public Utilisateur findByEmail(String email);
	public Utilisateur findByEmailAndPassword(String email,String password);
	
}