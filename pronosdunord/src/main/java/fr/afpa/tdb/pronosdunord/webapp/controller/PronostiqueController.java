package fr.afpa.tdb.pronosdunord.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PronostiqueController {
	
	@RequestMapping(value = {"accueil"},method = RequestMethod.GET)
	public String afficherAccueil() {
		return "acceuil";
		
	}
	@RequestMapping(value = {"pronos"},method = RequestMethod.GET)
	public String afficherPronistiques() {
		return "pronos_gratuits";
		
	}
	
	@RequestMapping(value = {"vip"},method = RequestMethod.GET)
	public String afficherDevenirVip() {
		return "devenir_vip";
		
	}
	@RequestMapping(value = {"historique"},method = RequestMethod.GET)
	public String afficherHistorique() {
		return "historique_pronos";
		
	}	

	@RequestMapping(value = {"faq"},method = RequestMethod.GET)
	public String afficherQuestionsEtContact() {
		return "questions_et_contact";
		
	}

	@RequestMapping(value = {"inscription"},method = RequestMethod.GET)
	public String afficherInscription() {
		return "inscription";
		
	}
	@RequestMapping(value = {"connexion"},method = RequestMethod.GET)
	public String afficherConnexion() {
		return "connexion";
		
	}
}
